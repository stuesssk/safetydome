#!/usr/bin/env python3
import cgi
from random import randint
from mysql_utils import get_connection

print("Content-type: text/html\n")

try:
    # Case statment within a sql query source from A Dow, sourced from
    # www.stackoverflow.com/questions/13887616/mysql-if-elseif-in-select-query
    my_query = """SELECT combatant.id, name,
    (
        CASE
            WHEN combatant.id NOT IN (SELECT winner_id FROM fight) THEN '0'
            WHEN combatant.id IN (SELECT winner_id FROM fight)
                              THEN COUNT(winner_id)
        END
    ) AS Win
        FROM combatant, fight
        WHERE combatant.id = winner_id
        OR combatant.id NOT IN (SELECT winner_id FROM fight)
        GROUP BY combatant.id
        ORDER BY Win DESC, name"""

    # Open connection
    connection = get_connection()
    cursor = connection.cursor()
    # Get the data
    cursor.execute(my_query)
    result = cursor.fetchall()
    # Close connection gathered all data
    cursor.close()
    connection.close()

    # Create key used to obfuscate the players id
    key = randint(100000, 1000000)

    print("<html><head><title>", 'Comabatants', "</title>")
    print("<link href='/css/list.css' type='text/css' rel='stylesheet' />")
    print("</head>")
    print("<body>")

    print("<a name='#top'>")
    # Give the user a link to the other lists and the homepage
    print("<p class='links'><a href= '/index.html'>Home</a>")
    print("<a href= '/cgi-bin/battle_list.py'>Battle Listings</a>")
    print("<a href= '/cgi-bin/ranking.py'>Combatants (Wins)</a>")
    print("<a href= '/cgi-bin/combatant_list.py'>Combatants (A-Z)</a>")
    print("<a href= '#bottom'>Bottom</a></p>")

    print("<h1 class=header>SafetyDome Combatants Ranking by Wins</h1>")

    print("<ul>")

    for players in result:
        # Before sending data over wire ofuscate the id numbers
        if int(players[2]) == 1:
            print("<li><a href=\"/cgi-bin/combatant_detail.py?page=",
                  (players[0] * key), '_', key, "\">", players[1],
                  int(players[2]), "Win", "</a></li><br>")
        else:
            print("<li><a href=\"/cgi-bin/combatant_detail.py?page=",
                  (players[0] * key), '_', key, "\">", players[1],
                  int(players[2]), "Wins", "</a></li><br>")
    print("</ul>")
    print("<a name='#bottom'>")
    # Give the user a link to the other lists and the homepage
    print("<p class='links'><a href= '/index.html'>Home</a>")
    print("<a href= '/cgi-bin/battle_list.py'>Battle Listings</a>")
    print("<a href= '/cgi-bin/ranking.py'>Combatants (Wins)</a>")
    print("<a href= '/cgi-bin/combatant_list.py'>Combatants (A-Z)</a>")
    print("<a href= '#top'>Top</a></p>")
    print("</body></html>")

except:
    # Redirect using meta tag sourced from:
    # https://stackoverflow.com/questions/5411538/redirect-from-an-html-page
    print("<html><meta http-equiv='refresh' content='0;"
          "url=/error_page.html'>")
