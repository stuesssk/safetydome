#!/usr/bin/env python3
import cgi
from mysql_utils import get_connection

print("Content-type: text/html\n")

try:

    fields = cgi.FieldStorage()
    ids = fields.getvalue("page")
    player_id, key = ids.split("_")

    # De ofuscate the id numbers
    player_id = str(int(player_id)/int(key))

    # Set up the query to get all the stats of the combatant
    my_query = """SELECT combatant.name AS 'Player Name',
               species.name AS 'Species',
               (base_atk + plus_atk) AS 'Attack Strength',
               (base_dfn + plus_dfn) AS 'Defense Strength',
               (base_hp + plus_hp) AS 'HP Strength'
               FROM species, combatant
               WHERE combatant.species_id = species.id
               AND combatant.id = %s"""
    # Open connection to server
    connection = get_connection()
    cursor = connection.cursor()
    # Get all the data I need
    cursor.execute(my_query, [player_id])
    result = cursor.fetchall()
    col_headers = cursor.description
    # Be nice to other users nd close conection when I'm done
    cursor.close()
    connection.close()

    # Having combatant name in title will raise an exception if the
    # query did not pull that any ID
    print("<html><head><title>", result[0][0], "</title>")
    print("<link href='/css/table.css' type='text/css' rel='stylesheet' />")
    print("</head>")

    print("<body>")

    print("<h1 class='header'>", result[0][0], "'s Statistics", "<h1>", sep='')

    print("<table border='1' cellpadding='10'>")
    print("<thead>")
    # Printing Column headers
    for cols in col_headers:
        print("<th>", cols[0], "</th>")
    print("</thead>")
    # Printing Cell data
    print("<tbody>")
    for row in result:
        print("<tr>")
        for cell in row:
            print("<td>", cell, "</td>")
        print("</tr>")
    print("</tbody>")
    print("</table>")

    # Print the links back to the other pages
    print("<p class='footer'><a href= '/index.html'>Home</a>")
    print("<a href= '/cgi-bin/battle_list.py'>Battle Listings</a>")
    print("<a href= '/cgi-bin/ranking.py'>Combatants (Wins)</a>")
    print("<a href= '/cgi-bin/combatant_list.py'>Combatants (A-Z)</a></p>")
    print("</body></html>")

except:
    # Redirect using meta tag sourced from:
    # https://stackoverflow.com/questions/5411538/redirect-from-an-html-page
    print("<html><meta http-equiv='refresh' content='0;"
          "url=/error_page.html'>")
