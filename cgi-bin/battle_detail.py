#!/usr/bin/env python3
import cgi
from mysql_utils import get_connection

print("Content-type: text/html\n")

try:
    fields = cgi.FieldStorage()
    ids = fields.getvalue("page")
    player1, player2, winner, key1, key2, key3 = ids.split("_")

    # De ofuscate the id numbers
    player1 = str(int(player1)/int(key1))
    player2 = str(int(player2)/int(key2))
    winner = str(int(winner)/int(key3))

    # Set up sql query string
    my_query = """SELECT n.name AS 'Player 1', m.name AS 'Player 2',
                w.name AS Winner, start AS 'Start Time',
                finish AS 'Finish Time' FROM fight, combatant AS n,
                combatant AS m, combatant
                AS w WHERE n.id = combatant_one
                AND m.id = combatant_two AND winner_id = w.id
                AND winner_id =%s AND combatant_one =%s AND
               combatant_two = %s"""
    # Open connection to server
    connection = get_connection()
    cursor = connection.cursor()
    # Gather all the data I need
    cursor.execute(my_query, [winner, player1, player2])
    result = cursor.fetchall()
    col_headers = cursor.description
    # Be nice and close the connection to the server when finished
    cursor.close()
    connection.close()

    print("<html>")
    print("<head>")
    print("<title>", result[0][0], "vs.", result[0][1], "</title></head>")
    print("<link href='/css/table.css' type='text/css' rel='stylesheet' />")
    print("</head>")
    # Print Title for the table
    print("<body>")
    print("<h1 class='header'>", result[0][0], "vs.", result[0][1], "<h1>")
    # Setup layout for table that I was unable to get working with CSS
    print("<table border='1' cellpadding='10'>")
    print("<thead>")
    # Print the data that was asked
    for cols in col_headers:
        print("<th>", cols[0], "</th>")
    print("</thead>")

    print("<tbody>")
    for row in result:
        print("<tr>")
        for cell in row:
            print("<td>", cell, "</td>")
        print("</tr>")
    print("</tbody>")
    print("</table>")

    # Print the links back to the other pages
    print("<p class='footer'><a href= '/index.html'>Home</a>")
    print("<a href= '/cgi-bin/battle_list.py'>Battle Listings</a>")
    print("<a href= '/cgi-bin/ranking.py'>Combatants (Wins)</a>")
    print("<a href= '/cgi-bin/combatant_list.py'>Combatants (A-Z)</a></p>")

    print("</body></html>")

except:
    # Redirect using meta tag sourced from:
    # https://stackoverflow.com/questions/5411538/redirect-from-an-html-page
    print("<html><meta http-equiv='refresh' content='0;"
          "url=/error_page.html'>")
