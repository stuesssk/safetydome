#!/usr/bin/env python3
import cgi
from random import randint
from mysql_utils import get_connection

try:
    my_query = "SELECT id, name FROM combatant ORDER BY name"
    # Open coonection
    connection = get_connection()
    cursor = connection.cursor()
    # Retrieve the data
    cursor.execute(my_query)
    result = cursor.fetchall()
    # Done with connection, close it so others can use
    cursor.close()
    connection.close()

    key = randint(100000, 1000000)

    print("Content-type: text/html\n")
    print("<html><head><title>", 'Combatants', "</title>")
    print("<link href='/css/list.css' type='text/css' rel='stylesheet' />")
    print("</head>")
    print("<body>")

    print("<a name='#top'>")
    # Give the user a link to the other lists and the homepage
    print("<p class='links'><a href= '/index.html'>Home</a>")
    print("<a href= '/cgi-bin/battle_list.py'>Battle Listings</a>")
    print("<a href= '/cgi-bin/ranking.py'>Combatants (Wins)</a>")
    print("<a href= '/cgi-bin/combatant_list.py'>Combatants (A-Z)</a>")
    print("<a href= '#bottom'>Bottom</a></p>")

    # Printing Header
    print("<h1 class='header'>SafetyDome Combatants Alphabatized</h1>")
    # Printing the list of Combatants
    print("<ul>")
    for players in result:
        # Before sending data over wire ofuscate the id numbers
        print("<li><a href=\"/cgi-bin/combatant_detail.py?page=",
              (players[0] * key), '_', key, "\">",
              players[1], "</a></li><br>", sep='')
    print("</ul>")

    print("<a name='#bottom'>")
    # Give the user a link to the other lists and the homepage
    print("<p class='links'><a href= '/index.html'>Home</a>")
    print("<a href= '/cgi-bin/battle_list.py'>Battle Listings</a>")
    print("<a href= '/cgi-bin/ranking.py'>Combatants (Wins)</a>")
    print("<a href= '/cgi-bin/combatant_list.py'>Combatants (A-Z)</a>")
    print("<a href= '#top'>Top</a></p>")

    print("</body></html>")

except:
    # Redirect using meta tag sourced from:
    # https://stackoverflow.com/questions/5411538/redirect-from-an-html-page
    print("<html><meta http-equiv='refresh' content='0;"
          "url=/error_page.html'>")
