#!/usr/bin/env python3
import cgi
from random import randint
from mysql_utils import get_connection

print("Content-type: text/html\n")

try:
    # Set up SQL string
    my_query = """SELECT n.name AS 'Player 1', m.name AS 'Player 2',
                  combatant_one, combatant_two, winner_id, start, finish
                  FROM fight, combatant AS n, combatant AS m
                  WHERE n.id = combatant_one AND m.id = combatant_two"""
    # Open connection to server
    connection = get_connection()
    cursor = connection.cursor()
    # Gather any necessary data
    cursor.execute(my_query)
    result = cursor.fetchall()
    # Close connection like a good user when finished retrieving data
    cursor.close()
    connection.close()

    # Calculate keys used to obfuscate player id numbers
    key1 = randint(100000, 1000000)
    key2 = randint(100000, 1000000)
    key3 = randint(100000, 1000000)

    print("<html><head><title>", 'Battles', "</title>")
    print("<link href='/css/list.css' type='text/css' rel='stylesheet' />")
    print("</head>")

    print("<body>")
    print("<a name='top'/>")
    print("<p class='links'><a href= '/index.html'>Home</a>")
    print("<a href= '/cgi-bin/battle_list.py'>Battle Listings</a>")
    print("<a href= '/cgi-bin/ranking.py'>Combatants (Wins)</a>")
    print("<a href= '/cgi-bin/combatant_list.py'>Combatants (A-Z)</a>")
    print("<a href= '#bottom'>Bottom</a></p>")
    print("<h1 class='header'>SafetyDome Battle List</h1>")

    print("<ul>")
    for battles in result:
        # Before sending data over wire ofuscate the id numbers
        # Will still need to send keys with query request
        print("<li><a href=\"/cgi-bin/battle_detail.py?page=",
              (battles[2] * key1), "_",
              (battles[3] * key2), "_",
              (battles[4] * key3), "_",
              key1, "_", key2, "_", key3, "\">",
              battles[0], " vs. ", battles[1], "</a></li><br>",
              sep='')

    print("</ul>")
    print("<a name='#bottom'>")
    # Give the user a link to the other lists and the homepage
    print("<p class='links'><a href= '/index.html'>Home</a>")
    print("<a href= '/cgi-bin/battle_list.py'>Battle Listings</a>")
    print("<a href= '/cgi-bin/ranking.py'>Combatants (Wins)</a>")
    print("<a href= '/cgi-bin/combatant_list.py'>Combatants (A-Z)</a>")
    print("<a href= '#top'>Top</a></p>")
    print("</body></html>")

except:
    # Redirect using meta tag sourced from:
    # https://stackoverflow.com/questions/5411538/redirect-from-an-html-page
    print("<html><meta http-equiv='refresh' content='0;"
          "url=/error_page.html'>")
